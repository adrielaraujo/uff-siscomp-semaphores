from threading import Semaphore, Thread
from random import randint
from time import sleep

class SemaphoreControl:
    def __init__(self, n):
        self.start(n)

    def controlSemaphores(self):
        """
        Cria os semáforos de controle
        """
        self.generalSemaphore = Semaphore()
        c1 = Semaphore()
        c2 = Semaphore()
        c3 = Semaphore()

        return c1,c2,c3

    def start(self, n):
        """
        Cria as treads e mapea os semafaros
        :param n: Quantidade de vezes que cada cor será impressa
        """
        c1, c2, c3 = self.controlSemaphores()

        ### Threads de cada cor
        # O método printColor é associado à execução de cada thread
        # Esta execução é automaticamente invocada ao iniciar (start) a execução da thread
        # Além disso, cada thread conhece  o semaforo relativo à execução posterior a sua
        red_Thread =  Thread(target = self.printColor, args=(n, "RED THREAD", c1, c2))
        blue_Thread = Thread(target = self.printColor, args=(n, "BLUE THREAD", c2, c3))
        gren_Thread = Thread(target = self.printColor, args=(n, "GREEN THREAD", c3, c1))

        ### Somente o primeiro semaforo continuará livre.
        # Estes Acquires garantem que as threads que os semaforos
        # estejam associadas não executem antes do momento correto
        c2.acquire()
        c3.acquire()

        red_Thread.start()
        blue_Thread.start()
        gren_Thread.start()

        red_Thread.join()
        blue_Thread.join()
        gren_Thread.join()


    def printColor(self, n, color, current, next):
        """
        Método genérico para imprimir qualquer cor!
        :param n: Quantidade de vezes que a cor será impressa
        :param color: String com um text informativo sobre a cor
        :param current: semaforo que controla a impressao da cor em questão
        :param next:  semáforo que controla a impressão da próxima cor
        """
        for i in range(n):
            #Acquire no semáforo que controla a impressão da cor corrente
            current.acquire()

            #Acquire no semaforo que controla a ordem de execução
            self.generalSemaphore.acquire()

            #Sleep com tempo aleatorio dado entre 0 e 9 segundos a uma dada cor
            self.sleeper(i,randint(0, 9), color)

            #Libera o semáforo que controla a ordem de execução
            self.generalSemaphore.release()

            print("Done\n")

            #Libera semáforo que controla a impressao da próxima cor
            next.release()

    def sleeper(self, nCurrent, i, treadColor):
        '''
        Coloca uma thread para dormir por um dado período de tempo

        :param nCurrent: Indica qual o índice da execução corrente
        :param i: indica a quantidaded de segundos que a trhread irá dormir
        :param treadColor: String informativa da thread
        '''

        print ("N = %d | %s \nSleeping for %d seconds" % (nCurrent + 1, treadColor, i))
        sleep(i)
        print("Woke up")

if __name__=='__main__':
    n = int(input("||     Welcome      ||\n\nEnter de N value to start Program: "))
    print("\nStarting Program...\n\n:::::::::::::::::::\n")
    program = SemaphoreControl(n)

    input('Press any key to exit..')